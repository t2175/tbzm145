# M145 - Informatik Modul 🛠️🔗
▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░▒▒▒▒░░░▒▒▒▒░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒░▒▒▒▒▒▒░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒░░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒░░░░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░▒░░░░░░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒░▒▒▒░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒░░░░░▓▓
▓▓░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒░░░░░░▓▓
▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
_______▒__________▒▒▒▒▒▒▒▒▒▒▒▒▒▒
______▒_______________▒▒▒▒▒▒▒▒
_____▒________________▒▒▒▒▒▒▒▒
____▒___________▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
___▒
__▒______▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
_▒______▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓
▒▒▒▒___▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓
▒▒▒▒__▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓▒▓
▒▒▒__▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
▒▒

## Modulinformationen
- **Modul:** M145
- **Dozent:** Jonas Van Essen
- **Schüler:** Philipp Schoch

## Ablauf des Moduls

### Block 01 (23.02.2024) 
- Modul intro - 1L - ✔️
- Repetition - M117 / M129 - ✔️
- BeASwitch - 1L - ✔️
- BeARouter - 2L - ✔️

### Block 02 (01.03.2024) 
- Repetition - ✔️
- IPv4 Quiz - 2L - ✔️
- Start von Cisco Packet Tracer Übungen - 2L

### Block 03 (08.03.2024)
- Repetition
- Cisco Packet Tracer Übungen - 4L
- Lösen der Quiz als Vorbereitung zur LB1

### Block 04 (15.03.2024)
- LB1 - Repetition - 1L
- Netzwerkdokumentation - 1L
  - Theorie
  - Übung
- Monitoring - 2L
  - Theorie
  - Wireshark Übung

### Block 05 (22.03.2024)
- VLAN
  - Theorie - 1L
  - BeASwitch mit VLAN - 1L
  - Cisco Übung - 2L

### Ausfall (29.03.2024)
- Schulfrei

### Block 06 (05.04.2024)
- VLAN
  - Cisco Übung - 4L

### Block 07 (12.04.2024)
- LB2 - Switching / Routing / VLAN - 1L
- WLAN
  - Theorie - 1L
  - Haus ausmessen - 1L

### Block 08 (19.04.2024)
- VPN - 3L
- Troubleshooting

## Hinweise
- Den Status der erledigten Aufgaben kannst du durch Haken oder andere Symbole markieren.
- Externe Programme können mit Links oder Icons vermerkt werden.

---

*Hinweis: Gestalte die Symbole und Formatierungen nach deinem Geschmack und füge gegebenenfalls weitere Informationen oder Anpassungen hinzu.*
